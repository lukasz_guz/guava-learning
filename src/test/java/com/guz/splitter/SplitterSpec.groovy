package com.guz.splitter

import com.google.common.base.Splitter
import spock.lang.Specification

class SplitterSpec extends Specification {

    List<String> listOfStrings = ["Mateusz", "Tomek", "Zbyszek"]
    Map<String,String> map = ["First" : '1', "Second" : '2']

    def "Should return split String using pipe separator"() {
        when:
            Iterable<String> result = Splitter.on("|").split('Mateusz|Tomek|Zbyszek')

        then:
            result.asList() == listOfStrings
    }

    def "Should return split String using pipe separator with omit empty value"() {
        when:
            Iterable<String> result = Splitter.on("|").omitEmptyStrings().split('Mateusz|Tomek||Zbyszek|')

        then:
            result.asList() == listOfStrings
    }

    def "Should return map transform map to string separate pipe"() {
        when:
            Map<String,String> result = Splitter.on("|").withKeyValueSeparator("=").split('First=1|Second=2')

        then:
            result == map
    }
}
