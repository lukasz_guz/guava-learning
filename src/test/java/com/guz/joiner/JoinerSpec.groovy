package com.guz.joiner

import com.google.common.base.Joiner
import spock.lang.Specification

class JoinerSpec extends Specification {

    def "Should return concat list of stings separate pipe"() {
        given:
            List<String> listOfStrings = ["Mateusz", "Tomek", "Zbyszek"]

        when:
            String result = Joiner.on("|").join(listOfStrings)

        then:
            result == "Mateusz|Tomek|Zbyszek"
    }

    def "Should return concat list of stings separate pipe skips nulls"() {
        given:
            List<String> listOfStrings = ["Mateusz", "Tomek", null, "Zbyszek", null]

        when:
            String result = Joiner.on("|").skipNulls().join(listOfStrings)

        then:
            result == "Mateusz|Tomek|Zbyszek"
    }

    def "Should throw NullPointerException when skipNulls is not set"() {
        given:
            List<String> listOfStrings = ["Mateusz", "Tomek", null, "Zbyszek", null]

        when:
            Joiner.on("|").join(listOfStrings)

        then:
            thrown(NullPointerException)
    }

    def "Should return concat list of stings separate pipe and replace null double '-'"() {
        given:
            List<String> listOfStrings = ["Mateusz", "Tomek", null, "Zbyszek", null]

        when:
            String result = Joiner.on("|").useForNull("--").join(listOfStrings)

        then:
            result == "Mateusz|Tomek|--|Zbyszek|--"
    }

    def "Should return transform map to string separate pipe"() {
        given:
            Map<String,Integer> map = ["First" : 1, "Second" : 2]

        when:
            String result = Joiner.on("|").withKeyValueSeparator("=").join(map)

        then:
            result == "First=1|Second=2"
    }
}
